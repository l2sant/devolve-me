const mongoose = require('mongoose')
const Item = require('../models/item')
const Emprestimo = require("../models/emprestimo")


const itens = async(req, res) => {
    const item = await Item.find({})
             
    res.render('item/index', {
        itens: item,
        error: false
        })
}

const addItem = async(req, res) => {
    const itens = await Item({
        _id: new mongoose.Types.ObjectId(),
        nome: req.body.item
    })
    const duplicado = await Item.where("nome", req.body.item)
    const item = await Item.find({})
    if(duplicado.length > 0) {
        res.render('item/form', {
            itens: item ,
            error: false,
            duplicado:true
        })
    }else {
        itens.save()
        res.redirect('/itens')
    }
    
}


const formItem = async(req, res) => {
    const item = await Item.find({})

    res.render('item/form', {
        itens: item,
        error: false,
        duplicado:false
    })
}

const delItem = async(req, res) => {
    const emprestimo = await Emprestimo.findOne({item: req.params.id})

    if(emprestimo){
        const docs = await Item.find({})
                
        res.render('item/index', {
            itens: docs,
            error: true,
            amigo: emprestimo.nomeAmigo,
            duplicado: false
        }) 
        
    }
    else {
        await Item.deleteOne({_id: req.params.id})
                  .then(() => res.redirect('/itens'))
    }             
}


module.exports = {
    addItem,
    formItem,
    itens,
    delItem
}