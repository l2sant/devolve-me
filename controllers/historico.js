const Historico = require('../models/historico')
const Emprestimo = require('../models/emprestimo')

const moment = require('moment')

const index = async(req, res) => {
    const historicos = await Historico.find({})
    res.render('historico/index', {historicos})
}


const addHistorico = async(req, res) => {
    const emprestimo = await Emprestimo.findByIdAndDelete({_id: req.params.id}).populate('item')
                  
    const historico = Historico({
        item: emprestimo.item.nome,
        devolvido: moment(new Date()).format('DD/MM/YYYY'),
        amigo: emprestimo.nomeAmigo
        })
        historico.save()
        res.redirect('back')
}


module.exports = {
    index,
    addHistorico
}