const Emprestimo = require('../models/emprestimo')
const Item =require('../models/item')
const mongoose = require('mongoose')
const moment = require('moment')
const Joi = require('joi')



const home = async(req, res) => {
    const emprest = await Emprestimo.find({}).populate('item') 
    res.render('index', {emprest, moment})
}


const addEmprestimo = async(req, res) => {

    const reqs = {
        _id: mongoose.Types.ObjectId(),
        nomeAmigo: req.body.nomeAmigo,
        data: req.body.dtEmprestimo,
        devolucao: req.body.dtDevolucao,
        item: req.body.item
    }
   
    const validateData = {
        nomeAmigo: req.body.nomeAmigo,
        data: req.body.dtEmprestimo,
        devolucao: req.body.dtDevolucao,
        item: req.body.item
    } 

    const schema = Joi.object().keys({
        nomeAmigo: Joi.string().regex(/[^0-9]/).required(),
        data: Joi.date().required(),
        devolucao: Joi.date().min(Joi.ref('data')).allow(null).allow(''),
        item: Joi.string()
    })


    Joi.validate(validateData, schema, async(err, value) => {
        const itens = await Item.find({})
        const empres = await Emprestimo.where("item").equals(value.item)
        
        if(err) {
            res.render('formularioEmpres', {itens, err, reqs, errDuplicacao:false})
        }
        if(empres.length > 0) {
            res.render('formularioEmpres', {itens, err, reqs, errDuplicacao:true})
        }
        else {
            const emprestimo = await Emprestimo(reqs)
            emprestimo.save()              
            res.redirect('/')
        }
    }) 
}


const formAddEmpres = async(req, res) => {
    const itens = await Item.find({})
    
    res.render('formularioEmpres' ,{itens,
        err: false,
        reqs: false,
        errDuplicacao:false
    })
}


const delEmprestimo = async(req, res) => {
    await Emprestimo.deleteOne({'_id': req.params.id})
                    .then(() => res.redirect('back'))
}



module.exports = {
    home,
    addEmprestimo,
    formAddEmpres,
    delEmprestimo
}