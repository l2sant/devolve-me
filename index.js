const express = require('express')
const app = express()
const mongoose = require('mongoose')

const path = require('path')
const port = process.env.PORT || 3000
const mongo = process.env.MONGODB || 'mongodb://localhost:27017/devolve'

const routes = require('./routes/index')

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.urlencoded({extended: true}))
app.use('/', routes)

mongoose
    .connect(mongo, { useNewUrlParser: true })
    .then(() => {
        app.listen(port, () => console.log("server on!"))
    })