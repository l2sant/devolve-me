const express = require('express')
const router = express.Router()

const { home, addEmprestimo, formAddEmpres, delEmprestimo } = require('../controllers/emprestimo')
const { itens, addItem, formItem, delItem } = require('../controllers/item')
const { index, addHistorico } = require('../controllers/historico')

router.get('/', home)
router.get('/formEmprestimo', formAddEmpres)
router.post('/emprestimo',addEmprestimo)
router.get('/delEmprestimo/:id', delEmprestimo)

router.get('/itens', itens)
router.post('/item', addItem)
router.get('/formItem', formItem)
router.get('/del/:id', delItem)

router.get('/historico', index)
router.get('/addHistorico/:id', addHistorico)

module.exports = router