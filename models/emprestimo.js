const { Schema, model } = require('mongoose')

const emprestimoSchema = Schema({
    _id: Schema.Types.ObjectId,
    nomeAmigo: {type: String, required:true},
    data: {type: String ,required:true},
    devolucao: {type: String, default: ''},
    item: {type: Schema.Types.ObjectId, ref: "Item", required: true},
    devolvido: {type: Boolean, default: false}
})


module.exports = model('Emprestimo', emprestimoSchema)