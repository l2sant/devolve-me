const {Schema, model} = require('mongoose')


const histoSchema =  Schema({
    item: {type: String, required: true},
    devolvido: {type: String, required: true},
    amigo: {type: String, required: true},
})



module.exports = model('Historico', histoSchema)