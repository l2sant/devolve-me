const {Schema, model} = require('mongoose')

const itemSchema = Schema({
    _id: Schema.Types.ObjectId,
    nome: {type: String, required: true}
})


module.exports = model('Item', itemSchema)